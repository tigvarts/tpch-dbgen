#!/bin/bash

for ((i = 1; i < 23; ++i)); do
  echo -n Query $i
  time ./qgen $i | psql -d tpch > /dev/null;
done
