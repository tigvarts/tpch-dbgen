#!/bin/bash

echo "Compiling sources..."
make
echo
echo "Making database..."
./dbgen -s ${1:-1}
mv region.tbl nation.tbl customer.tbl supplier.tbl part.tbl partsupp.tbl orders.tbl lineitem.tbl /tmp
psql -d postgres -c "drop database if exists tpch"
psql -d postgres -c "create database tpch"
psql -d tpch < postgres_dll.sql
psql -d tpch < postgres_load.sql
psql -d tpch < postgres_ri.sql
rm /tmp/region.tbl /tmp/nation.tbl /tmp/customer.tbl /tmp/supplier.tbl /tmp/part.tbl /tmp/partsupp.tbl /tmp/orders.tbl /tmp/lineitem.tbl
echo "Database made successfully."
